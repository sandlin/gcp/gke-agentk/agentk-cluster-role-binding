# agentk-network

1. Modify terraform.tfvars
1. Modify install.sh
1. Execute install.sh
1. Take note of output. It'll be needed in the next project(s)

Sample Output: 
```
$ ./install.sh create
...
Outputs:

firewall = "agentk-firewall"
subnet = "agentk-subnet"
vpc = "agentk-vpc"
```

